import Axios from 'axios'
import Vue from 'vue'

const instance = Axios.create({
  baseURL: 'https://api.ghd.w3msys.com/api',
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  }
})

instance.interceptors.request.use((config) => {
  let apiToken = Vue.auth.getToken()
  if (apiToken && !config.headers.common.Authorization) {
    config.headers.common.Authorization = `Bearer ${apiToken}`
  }
  NProgress.start()
  // console.log(config)
  return config
})

instance.interceptors.response.use(response => {
  NProgress.done()
  return response
}, error => {
  NProgress.done()
  return error
})


export default instance

export default (Vue) => {
  Vue.auth = {
    getToken () {
      const token = sessionStorage.getItem('ghd-hm-token')
      if (!token) {
        return null
      }
      return token
    },
    setToken (token) {
      sessionStorage.setItem('ghd-hm-token', token)
    },
    destroyToken () {
      sessionStorage.removeItem('ghd-hm-token')
    },
    isAuthenticated () {
      if (this.getToken()) {
        return true
      } else {
        return false
      }
    }
  }

  Object.defineProperties(Vue.prototype, {
    $auth: {
      get () {
        return Vue.auth
      }
    }
  })
}

export default {
    CLEAR_STATE (state) {
      state.user = {}
      state.hotel = {}
    },
    
    SET_USER (state, payload) {
      state.user = payload
    },

    SET_HOTEL (state, payload) {
        state.hotel = payload
    },

    UPDATE_HOTEL (state, payload) {
      state.hotel = payload
    },

    SET_GALLERY (state, payload) {
      state.gallery = payload
    },

    SET_ROOM_TYPES (state, payload) {
      state.roomTypes = payload
    },

    ADD_TO_GALLERY (state, payload) {
      state.gallery.unshift(payload)
    },

    DELETE_FROM_GALLERY (state, payload) {
      for(let i = 0; i < state.gallery.length; i++) {
        if (state.gallery[i].id === payload) {
          state.gallery.splice(i, 1)
          return
        }
      }
    }
  }
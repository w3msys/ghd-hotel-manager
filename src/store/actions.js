import axios from './../config/http';


export default {
    
    clearState ({commit}) {
      commit('CLEAR_STATE')
    },

    setUser ({commit}) {
        axios({
            method: 'get',
            url: 'manager/auth/me'
        }).then(response => {
            commit('SET_HOTEL', response.data.hotel)
            delete response.data.hotel
            commit('SET_USER', response.data)
        })
    },

    setGallery ({commit}) {
        axios({
            method: 'get',
            url: 'manager/hotel/gallery'
        }).then(response => {
            // console.log(response.data)
            commit('SET_GALLERY', response.data)
        })
    },

    setRoomTypes ({commit}) {
        axios({
            method: 'get',
            url: 'manager/hotel/room_types'
        }).then(response => {
            commit('SET_ROOM_TYPES', response.data)
            // console.log(response.data)
        })
    },

    addToGallery ({commit}, payload) {
        commit('ADD_TO_GALLERY', payload)
    },

    deleteFromGallery ({commit}, payload) {
        commit('DELETE_FROM_GALLERY', payload)
    },

    updateHotel ({commit}, payload) {
        commit('UPDATE_HOTEL', payload)
    }
  }
export default {
    getUser (state) {
      return state.user
    },

    getHotel (state) {
      return state.hotel
    },

    getGallery (state) {
      return state.gallery
    }
  }
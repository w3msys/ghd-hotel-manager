import Vue from 'vue'
import Router from 'vue-router'
import Login from '../components/authentication/Login.vue'
import NotFound from './../components/authentication/404.vue'
import Auth from '../components/layouts/Auth.vue'
import Dashboard from '../components/dashboard/dashboard.vue'
import Rooms from './../components/rooms/roomTypes.vue'
import Managers from '../components/managers/managers.vue'
import Profile from '../components/profile/profile.vue'
import EditProfile from './../components/profile/editProfile.vue'
import RoomsDirectory from './../components/rooms/roomsDirectory.vue'
import EditRoomType from './../components/rooms/editRoomType.vue'
import EditRoom from './../components/rooms/editRoom.vue'
import Facilities from './../components/facilities/facilitiesHome.vue'
import AddFacility from './../components/facilities/addFacility.vue'
import AddRoom from './../components/rooms/addRoom.vue'
import Gallery from './../components/gallery/gallery.vue'
import AddRoomType from './../components/rooms/addRoomType.vue'


Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login,
      meta: {title: 'Login'}
    },
    {
      path: '/404',
      name: 'not-found',
      component: NotFound,
      meta: {title: 'Oops'}
    },
    {
      path: '/auth',
      name: 'Auth',
      meta: {auth: true},
      component: Auth,
      children: [
        {
          name: 'Dashboard',
          path: 'dashboard',
          component: Dashboard,
          meta: {title: 'Dashboard'}
        },
        {
          name: 'Rooms',
          path: 'rooms-types',
          meta: {title: 'Room Types'},
          component: Rooms,
          props: true
        },
        {
          name: 'editRoomType',
          path: 'rooms/edit-room-type',
          meta: {title: 'Edit Room Type'},
          component: EditRoomType,
          props: true
        },
        {
          name: 'addRoomType',
          path: 'rooms/add-room-type',
          meta: {title: 'Add Room Type'},
          props: true,
          component: AddRoomType
        },
        {
          name: 'editRoom',
          path: 'rooms/edit-room',
          component: EditRoom,
          meta: {title: 'Edit Room'},
          props: true
        },
        {
          name: 'addRoom',
          path: 'rooms/add-room',
          component: AddRoom,
          meta: {title: 'Add New Room'}
        },
        {
          name: 'RoomsDirectory',
          path: 'rooms/directory',
          component: RoomsDirectory,
          meta: {title: 'Room Directory'},
          props: true
        },
       
        {
          name: 'profile',
          path: 'profile',
          component: Profile,
          meta: {title: 'Edit Your Profile'},
          props: true
        },
        {
          name: 'editProfile',
          path: 'profile/edit',
          meta: {title: 'Edit Hotel Information'},
          component: EditProfile,
          props: true
        },
        {
          name: 'facilities',
          path: 'facilities',
          component: Facilities,
          meta: {title: 'Facilities'},
          props: true
        },
        {
          name: 'addFacility',
          path: 'facilities/add',
          component: AddFacility,
          meta: {title: 'Add a new facility'}
        },
        {
          name: 'gellery',
          path: 'gallery',
          component: Gallery,
          meta: {title: 'View Gallery'}
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {

  if (to.matched.some(item => !!item.meta.auth)) {
    if (Vue.auth.isAuthenticated()) {
      next()
      $('title').text(`${to.meta.title} | HOTEL MANAGER`)
    } else {
      next({path: '/', query: {redirect: to.fullPath}})
    }
  } else {
    next()
  }
})

router.afterEach((to, from) => {

})
export default router
